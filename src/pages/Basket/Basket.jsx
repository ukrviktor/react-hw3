import React from 'react';
import PropTypes from 'prop-types';
import { useOutletContext } from 'react-router-dom';
import styles from './Basket.module.scss';
import { Product } from '../../components/Product/Product';

export const Basket = () => {
  const {
    products,
    favorites,
    handleFavoritesClick,
    productsInBasket,
    handleBasketClick,
    setModal,
    setModalData,
  } = useOutletContext();

  const productsBasket = products.filter((product) =>
    productsInBasket.includes(product.id)
  );

  return (
    <div className={styles.Basket}>
      {!productsBasket.length ? (
        <h1>Доданих товарів немає</h1>
      ) : (
        <ul className={styles.BasketList}>
          {productsBasket.map((product) => (
            <Product
              key={product.id}
              product={product}
              favorites={favorites}
              handleFavoritesClick={handleFavoritesClick}
              productsInBasket={productsInBasket}
              handleBasketClick={handleBasketClick}
              setModal={setModal}
              setModalData={setModalData}
            ></Product>
          ))}
        </ul>
      )}
    </div>
  );
};

Basket.propTypes = {
  product: PropTypes.shape({}).isRequired,
  favorites: PropTypes.array.isRequired,
  handleFavoritesClick: PropTypes.func.isRequired,
  productsInBasket: PropTypes.array.isRequired,
  handleBasketClick: PropTypes.func.isRequired,
  setModal: PropTypes.func.isRequired,
  setModalData: PropTypes.func.isRequired,
};
