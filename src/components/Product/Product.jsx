import React from 'react';
import styles from './Product.module.scss';
import PropTypes from 'prop-types';
import { StarIco } from '../../UI/Icons/StarIco/StarIco';
import { StarEmptyIco } from '../../UI/Icons/StarEmptyIco/StarEmptyIco';
import { BasketIco } from '../../UI/Icons/BasketIco/BasketIco';
import { BasketEmptyIco } from '../../UI/Icons/BasketEmptyIco/BasketEmptyIco';

export const Product = ({
  product,
  favorites,
  handleFavoritesClick,
  productsInBasket,
  handleBasketClick,
  setModal,
  setModalData,
}) => {
  const { id, name, price, imgUrl, article, color } = product;
  const isFavorites = favorites.includes(id);
  const isBasket = productsInBasket.includes(id);

  // create content for modal add product
  const modalDataAddProduct = {
    header: 'Бажаєте додати товар до корзини ?',
    closeButton: true,
    name,
    price,
    action: [
      {
        text: 'Ok',
        backgroundColor: '#1c8646',
        onClick: () => handleBasketClick(id),
      },
      {
        text: 'Cancel',
        backgroundColor: '#1c8646',
        onClick: null,
      },
    ],
  };

  // create content for modal delete product
  const modalDataDeleteProduct = {
    header: 'Бажаєте видалити товар з корзини ?',
    closeButton: true,
    name,
    price,
    action: [
      {
        text: 'Ok',
        backgroundColor: '#cc1934',
        onClick: () => handleBasketClick(id),
      },
      {
        text: 'Cancel',
        backgroundColor: '#cc1934',
        onClick: null,
      },
    ],
  };

  return (
    <>
      <div className={styles.ProductContainer}>
        <div className={styles.Product}>
          <div className={styles.body}>
            <div className={styles.img}>
              <img src={`./img/headphones/${imgUrl}`} alt="" />
            </div>
            <div className={styles.title}>{name}</div>
            <div className={styles.article}>
              <span className={styles.articleText}>Артикуль</span>:&nbsp;
              <span className={styles.articleValue}>{article}</span>
            </div>
            <div className={styles.color}>
              <span className={styles.colorText}>Цвет</span>:&nbsp;
              <span className={styles.colorValue}>{color}</span>
            </div>
          </div>
          <div className={styles.bottom}>
            <div className={styles.actions}>
              <span className={styles.basket}>
                {isBasket ? (
                  <span
                    onClick={() => {
                      // handleBasketClick(id);
                      setModalData(modalDataDeleteProduct);
                      setModal(true);
                    }}
                  >
                    <BasketEmptyIco width={26} fill={'#cc1934'} />
                  </span>
                ) : (
                  <span
                    onClick={() => {
                      setModalData(modalDataAddProduct);
                      setModal(true);
                    }}
                  >
                    <BasketIco width={26} fill={'#1c8646'} />
                  </span>
                )}
              </span>
              <span className={styles.star}>
                {isFavorites ? (
                  <span
                    onClick={() => {
                      handleFavoritesClick(id);
                    }}
                  >
                    <StarIco width={26} fill={'#ffda12'} />
                  </span>
                ) : (
                  <span
                    onClick={() => {
                      handleFavoritesClick(id);
                    }}
                  >
                    <StarEmptyIco width={26} fill={'#ffda12'} />
                  </span>
                )}
              </span>
            </div>
            <div className={styles.price}>
              <span className={styles.sum}>{price}</span>
              <span className={styles.currency}> ₴</span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

Product.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string,
    price: PropTypes.number,
    imgUrl: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
  }).isRequired,
  favorites: PropTypes.array.isRequired,
  handleFavoritesClick: PropTypes.func.isRequired,
  productsInBasket: PropTypes.array.isRequired,
  handleBasketClick: PropTypes.func.isRequired,
  setModal: PropTypes.func.isRequired,
  setModalData: PropTypes.func.isRequired,
};

Product.defaultProps = {
  product: {
    name: '',
    price: 0,
    imgUrl: '',
    article: '',
    color: '',
  },
};
