import React from 'react';
import { Outlet } from 'react-router-dom';
import { Loader } from '../../UI/Loader/Loader';
import styles from './MainRight.module.scss';

export const MainRight = ({
  isDataLoading,
  fetchError,
  products,
  favorites,
  handleFavoritesClick,
  productsInBasket,
  handleBasketClick,
  setModal,
  setModalData,
}) => {
  return (
    <div className={styles.MainRight}>
      {fetchError && <h2>Error: ${fetchError}</h2>}
      {isDataLoading ? (
        <Loader />
      ) : (
        <Outlet
          context={{
            products,
            favorites,
            handleFavoritesClick,
            productsInBasket,
            handleBasketClick,
            setModal,
            setModalData,
          }}
        />
      )}
    </div>
  );
};
