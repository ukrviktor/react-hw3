import React, { useState, useEffect } from 'react';
import { useFeching } from '../../hooks/useFetching';
import { Service } from '../../API/Services';
import { Layout } from '../../Layout/Layout';

// load favorites from localStorage
const favoriteIdsFromStorage = localStorage.getItem('favorites');
// load products in basket from localStorage
const basketIdsFromStorage = localStorage.getItem('basket');

export const App = () => {
  // state products
  const [products, setProducts] = useState([]);

  /********************** FAVORITES **********************/
  // state favorites
  const [favorites, setFavorites] = useState([]);

  // add/remove favorites id
  const handleFavoritesClick = (id) => {
    if (favorites.includes(id)) {
      setFavorites((state) => state.filter((currentId) => currentId !== id));
    } else {
      setFavorites((state) => [...state, id]);
    }
  };

  // set the favorites when it changes
  useEffect(() => {
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }, [favorites]);

  /********************** BASKET **********************/
  // state products in basket
  const [productsInBasket, setProductsInBasket] = useState([]);

  // add/remove productId in basket
  const handleBasketClick = (id) => {
    if (productsInBasket.includes(id)) {
      setProductsInBasket((state) =>
        state.filter((currentId) => currentId !== id)
      );
    } else {
      setProductsInBasket((state) => [...state, id]);
    }
  };

  // set the basket when it changes
  useEffect(() => {
    localStorage.setItem('basket', JSON.stringify(productsInBasket));
  }, [productsInBasket]);

  /********************** LOAD PRODUCTS **********************/
  // get data
  const [fetchData, isDataLoading, fetchError] = useFeching(async () => {
    const data = await Service.getAll();
    setProducts(data.headphones);
  });

  /********************** MOUNTING COMPONENT **********************/
  // load when App is mounting
  useEffect(() => {
    // load products
    fetchData();

    // load favorites
    if (favoriteIdsFromStorage) {
      setFavorites(JSON.parse(favoriteIdsFromStorage));
    }

    // load basket
    if (basketIdsFromStorage) {
      setProductsInBasket(JSON.parse(basketIdsFromStorage));
    }
  }, []);

  /********************** RENDER **********************/
  return (
    <Layout
      isDataLoading={isDataLoading}
      fetchError={fetchError}
      products={products}
      favorites={favorites}
      handleFavoritesClick={handleFavoritesClick}
      productsInBasket={productsInBasket}
      handleBasketClick={handleBasketClick}
    />
  );
};
