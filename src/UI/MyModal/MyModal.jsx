import React from 'react';
import PropTypes from 'prop-types';
import styles from './MyModal.module.scss';
import { Button } from '../Button/Button';

export function MyModal({ modalData, visible, setVisible }) {
  const { header, closeButton, name, price, action } = modalData;

  // function for close modal
  const closeModal = () => setVisible(false);

  // create class for modal
  const rootClasses = [styles.myModal];
  if (visible) rootClasses.push(styles.active);

  return (
    <div className={rootClasses.join(' ')} onClick={closeModal}>
      <div
        className={styles.myModalContent}
        onClick={(e) => e.stopPropagation()}
      >
        <div className={styles.myModalHeader}>
          <h1 className={styles.myModalTitle}>{header}</h1>
          {closeButton && (
            <div className={styles.myModalCloseBtn} onClick={closeModal}>
              ✖
            </div>
          )}
        </div>
        <div className={styles.myModalBody}>
          <div className={styles.modalAddBasket}>
            <div className={styles.title}>{name}</div>
            <div className={styles.price}>
              <span className={styles.sum}>{price}</span>
              <span className={styles.currency}> ₴</span>
            </div>
          </div>
        </div>
        <div className={styles.myModalAction}>
          {action.map((btn) => (
            <Button
              key={btn.text}
              text={btn.text}
              backgroundColor={btn.backgroundColor}
              onClick={() => {
                btn.onClick && btn.onClick();
                closeModal();
              }}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

MyModal.propTypes = {
  modalData: PropTypes.shape({
    header: PropTypes.string.isRequired,
    closeButton: PropTypes.bool,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    action: PropTypes.shape({}),
  }),
};
